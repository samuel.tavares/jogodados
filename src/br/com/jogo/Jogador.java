package br.com.jogo;

import java.util.List;

public class Jogador {

    public int somar(int grupo[]){
        int soma = 0;
        for (int numero : grupo) {
            soma += numero;
        }
        return soma;
    }
}
