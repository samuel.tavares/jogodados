package br.com.jogo;

import java.util.List;

public class Dado {
    private int lado;
    private int dado [] = new int[6];

    public Dado(int lado) {
        this.lado = lado;
    }

    public int getLado() {
        return lado;
    }

    public void setLado(int lado) {
        this.lado = lado;
    }
}
